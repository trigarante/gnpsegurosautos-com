module.exports = {
  target: "static",
  /*
   ** Headers of the page
   */
  head: {
    title: "gnpsegurosautos.com",
    htmlAttrs: { lang: "es-MX" },
    meta: [
      { charset: "utf-8" },
      { hid: "robots", name: "robots", content: "index, follow" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "GNP" },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
  },
  /*
   ** Customize the progress bar color
   */
  loading: { color: "#3bd600" },
  /*
   ** Build configuration
   */
  build: {
    //analyze:true,
    /*
     ** Run ESLint on save
     */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          //loader: 'eslint-loader',
          exclude: /(node_modules)/,
        });
      }
    },
  },

  // include bootstrap css
  css: ["static/css/bootstrap.min.css", "static/css/styles.css"],
  plugins: [
    { src: "~/plugins/filters.js", ssr: false },
    { src: "~/plugins/apm-rum.js", ssr: false },
  ],
  modules: ["@nuxtjs/axios"],
  // gtm:{ id: 'GTM-PQ763SD' },
  buildModules: [["@nuxtjs/pwa"]],
  pwa: {
    meta: {
      title: "GNP Seguros",
      author: "NexosLab",
    },
    workbox: {
      runtimeCaching: [
        {
          urlPattern: "https://gnpsegurosautos.com/img/*",
          handler: "cacheFirst",
          method: "GET",
          strategyOptions: { cacheableResponse: { statuses: [0, 200] } },
        },
        {
          urlPattern: "https://gnpsegurosautos.com/_nuxt/*",
          handler: "cacheFirst",
          method: "GET",
          strategyOptions: { cacheableResponse: { statuses: [0, 200] } },
        },
      ],
    },
    manifest: {
      name: "GNP Seguros",
      short_name: "GNP",
      lang: "es",
      display: "standalone",
      theme_color: "#ff6e00",
      start_url: "/",
    },
  },
  env: {
    /*PRODUCCION*/
    tokenData: "mHf/0x8xqWmYlrjaRWECOzmkksnuNDZv1fBvMLjpI2g=",
    coreBranding: "https://dev.core-brandingservice.com",
    catalogo: "https://dev.web-gnp.mx",
    sitio: "https://p.gnpsegurosautos.com",
    hubspot:"https://core-hubspot-dev.mark-43.net/deals/landing",
    motorCobro: "https://p.gnpsegurosdeautos.mx/comprasegura/cliente/",
    coreRequest: "https://dev.core-persistance-service.com",
    urlValidaciones: "https://core-blacklist-service.com/rest", //PRODUCCIÓN
    urlGetConfiguracionCRM: "https://www.mark-43.net/mark43-service", //PRODUCCIÓN
    promoCore: "https://dev.core-persistance-service.com",
    Environment:'DEVELOP',
  },
  render: {
    http2: { push: true },
    resourceHints: false,
    compressor: { threshold: 9 },
  },
};
