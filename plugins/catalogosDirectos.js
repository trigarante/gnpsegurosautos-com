import axios from 'axios'

let catalogo = process.env.catalogo + "/v3/gnp-car";
class CatalogosDirectos {
  marcas() {
    return axios({
      method: "get",
      url: catalogo + '/brand'
    })
  }
  modelos(marca) {
    return axios({
      method: "get",
      url: catalogo + `/year?brand=${marca}`
    })
  }
  submarcas(marca, modelo) {
    return axios({
      method: "get",
      url: catalogo + `/model?brand=${marca}&year=${modelo}`
    }
    );
  }
  descripciones(marca, modelo, submarca) {
    return axios({
      method: "get",
      url: catalogo + `/variant?brand=${marca}&year=${modelo}&model=${submarca}`
    }
    );
  }
}


export default CatalogosDirectos;
